sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"./GeoServer"
	], function (Controller, MessageToast, GeoServer) {
	"use strict";
	var URL;
	var bRecZoomSelected;
	return {
		setMapConfiguration: function (oView, mapType) {
			if (mapType === "H") {
				URL = "https://mt.google.com/vt/lyrs=m&x={X}&y={Y}&z={LOD}"
			} else {
				URL = "https://mt.google.com/vt/lyrs=s&x={X}&y={Y}&z={LOD}"
			}
			/*	Google Map Api Configuration Starts for VBM Map Layer*/
			var oMapConfig = {
				"MapProvider": [{
					"name": "GMAP",
					"description": "Map Provider",
					"tileX": "256",
					"tileY": "256",
					"maxLOD": "20",
					"copyright": "Tiles Courtesy of Google Maps",
					"Source": [{
							"id": "s1",
							"url": URL
						}
]
				}],
				"MapLayerStacks": [{
					"name": "GOOGLE",
					"MapLayer": {
						"name": "layer2",
						"refMapProvider": "GMAP",
						"opacity": "6.0",
						"colBkgnd": "RGB(255,255,255)"
					}
				}]
			};
			//setting the MapConfiguration and LayerStack with GeoMap reference
			oView.byId("id_geomap")
				.setMapConfiguration(oMapConfig);
			oView.byId("id_geomap")
				.setRefMapLayerStack("GOOGLE");
		},
		enableRectangularSelection: function (oView, bToggle) {
			if (bToggle) {
				oView
					.byId("id_geomap")
					.setRectangularSelection(true);
				oView
					.byId("id_geomap")
					.setRectZoom(true);
				bRecZoomSelected = true;
			} else {
				oView
					.byId("id_geomap")
					.setRectZoom(false);
				bRecZoomSelected = false;
			}
			return bRecZoomSelected;
		},
		setLegend: function (oView) {
			if (oView.byId("id_geomap")
				.getLegendVisible() == true) {
				oView.byId("id_geomap")
					.setLegendVisible(false);
				oView.byId("btnLegend")
					.setTooltip("Show legend");
			} else {
				oView.byId("id_geomap")
					.setLegendVisible(true);
				oView.byId("btnLegend")
					.setTooltip("Hide legend");
			}
		},
		setMapType: function (oView, oBundle, evt) {
			var bTogglePressed = evt.getParameter("pressed");
			if (bTogglePressed) {
				oView.byId("id_tgle_map")
					.setText(oBundle
						.getText("hybrid"));
				this.setMapConfiguration(oView, "S");

			} else {
				oView.byId("id_tgle_map")
					.setText(oBundle
						.getText("satelite"));
				this.setMapConfiguration(oView, "H")
			}
		},
		setInitialHome: function (oView, aRailObject) {
			var bBoxValues = [];
			oView
				.byId("id_geomap")
				.setInitialPosition("7.851506300000;51.724960100000;0");
			oView
				.byId("id_geomap")
				.setZoomlevel(8);
			GeoServer.getGeoServerData(aRailObject, oView, bBoxValues);
			return false;
		},
		getBBOXSelection: function (oView, evt, aRailObject, bRecZoomSelected) {
			if (bRecZoomSelected) {
				var sLowerRight = evt.getParameter("viewportBB")
					.lowerRight;
				var sUpperLeft = evt.getParameter("viewportBB")
					.upperLeft;
				var sCenterPosition = evt.getParameter("centerPoint");
				var aLowerRight = sLowerRight.split(";");
				var aUpperLeft = sUpperLeft.split(";");
				var aBboxValue1 = [];
				aBboxValue1.push(aLowerRight[1]);
				aBboxValue1.push(aUpperLeft[0]);
				var sBBoxValue1 = aBboxValue1.join();
				var aBboxValue2 = [];
				aBboxValue2.push(aUpperLeft[1]);
				aBboxValue2.push(aLowerRight[0]);
				var sBBoxValue2 = aBboxValue2.join();
				var bBoxValues = [];
				bBoxValues.push(sBBoxValue1 + "," + sBBoxValue2);
				bBoxValues.push(sCenterPosition);
				GeoServer.getGeoServerData(aRailObject, oView, bBoxValues);
				oView
					.byId("id_tgbtn_seelction")
					.setPressed(false);
				oView
					.byId("id_geomap")
					.setRectangularSelection(false);
				oView
					.byId("id_geomap")
					.setRectZoom(false);
				bRecZoomSelected = false;
				return bRecZoomSelected;
			} else {
				return false;
			}
		}
	};
});