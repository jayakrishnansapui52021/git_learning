sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
    "com/db/de/elan/util/Formatter",
	"./SelectionController",
	"sap/m/MessageToast",
	"sap/ui/model/Filter",
	"./VariantManagement",
	"./Utility",
	"./ServiceControl",
	"./WeatherService",
	"./GeoServer",
	"./GeoMap",
	"com/db/de/elan/gis/GisPopover"
], function (BaseController, JSONModel, Formatter, SelectionController, MessageToast, Filter, VariantManagement, Utility, ServiceControl,
	WeatherService, GeoServer, GeoMap, GisPopover) {
	"use strict";
	var dFromDate;
	var dToDate;
	var aTplnrFilter;
	var aWorkPlaceFilter;
	var oView;
	var oBundle;
	var oPlantJsonModel;
	var oTplaceJsonModel;
	var oWplaceJsonModel;
	var oDataModel;
	var oGlobalBusyDialog;
	var oVariantDataModel
	var oGeoMap;
	var aGetNotificationFilters;
	var aNotificationType;
	var aNotificationPriority;
	var aNotificationStatus;
	var aDateRange;
	var dFromDate;
	var dToDate;
	var aPlant;
	var aFunctionalLocation;
	var aWorkPlace;
	var aRailObject;
	var iLongitude;
	var iLatitude;
	var bRecZoomSelected = false;
	var detailContent;
	return BaseController.extend("com.db.de.elan.controller.Master", {
		formatter: Formatter,
		onInit: function () {
			oView = this.getView();
			oGlobalBusyDialog = new sap.m.BusyDialog();
			oBundle = this._getResourceBundle();
			//default model is for ZMAI_UI5_ELAN service
			oDataModel = this.getOwnerComponent()
				.getModel();
			this.getLocalModel(); //loading local model from basecontroller
			//default model is for ZMAI_UI_VARIANTS_SRV service
			oVariantDataModel = this.getOwnerComponent()
				.getModel("variant");
			//calling the plant model at initally for the value help
			this._setupModelData();
			//calling Variant Collection from Backend to check for the Initial Variants Collections
			ServiceControl.getServiceData(oView, "R", true, oVariantDataModel, "", true);
			this.getRouter()
				.getRoute("master")
				.attachPatternMatched(this._onMasterMatched, this);
		},
		//setting the screen view
		_onMasterMatched: function () {
			//Set the layout property of the FCL control to 'OneColumn'
			this.getModel("appView")
				.setProperty("/layout", "OneColumn");
		},
		_setupModelData: function () {
			oPlantJsonModel = Utility.setupPlantModel(oDataModel, oView);
		},
		//getResource bundle method for getting i18n text content
		_getResourceBundle: function () {
			return this.getOwnerComponent()
				.getModel("i18n")
				.getResourceBundle();
		},
		//event for valuehelpPlant fragment and selection
		handleValueHelpPlant: function (oEvent) {
			var aInputPlant = [];
			this._valueHelpDialog = sap.ui.xmlfragment(
				"com.db.de.elan.util.Plant",
				this
			);
			//getting all the available tokens from the Plant Input Control
			var aSelectedPlants = oView.byId("id_mi_plant")
				.getTokens();
			for (var plant = 0; plant < aSelectedPlants.length; plant++) {
				aInputPlant.push(aSelectedPlants[plant].getText());
			}
			//aPlantCollections is an array of existing Plants
			var aPlantCollections = this.getView()
				.getModel("oPlantJsonModel")
				.getProperty("/PlantSet");
			for (var type = 0; type < aPlantCollections.length; type++) {
				aPlantCollections[type].selected = false;
			}
			//looping the values and adding the selected property as true for the selected tokens from the Input
			for (var inputValue = 0; inputValue < aInputPlant.length; inputValue++) {
				for (var type = 0; type < aPlantCollections.length; type++) {
					if (aPlantCollections[type].Key === aInputPlant[inputValue]) {
						aPlantCollections[type].selected = true;
					}
				}
				this.getView()
					.getModel("oPlantJsonModel")
					.setProperty("/PlantSet", aPlantCollections);
			}
			this._valueHelpDialog.setMultiSelect(true);
			this._valueHelpDialog.setRememberSelections(true);
			this._valueHelpDialog.setShowClearButton(true);
			this.getView()
				.addDependent(this._valueHelpDialog);
			this._valueHelpDialog.open();
		},
		//handle value help search event for plant fragment
		handleValueHelpSearchPlant: function (oEvent) {
			var sValue = oEvent.getParameter("value");
			var oFilter = new Filter("Key", sap.ui.model.FilterOperator.Contains, sValue);
			var oBinding = oEvent.getSource()
				.getBinding("items");
			oBinding.filter([oFilter]);
		},
		//handle value help close event for plant fragment
		handleValueHelpClosePlant: function (oEvent) {
			var oSelectedItems = oEvent.getParameter("selectedItems");
			var aTitle = [];
			if (oSelectedItems) {
				this.byId("id_mi_plant")
					.removeAllTokens();
				for (var title = 0; title < oSelectedItems.length; title++) {
					var text = oSelectedItems[title].getTitle();
					aTitle.push(text);
				}
				for (var plant = 0; plant < aTitle.length; plant++) {
					this.byId("id_mi_plant")
						.addToken(new sap.m.Token({
							text: aTitle[plant]
						}));
				}
				if (oSelectedItems.length === 0) {
					this.getView()
						.byId("id_fg_workplace")
						.setVisibleInFilterBar(false);
				} else {
					this.getView()
						.byId("id_fg_workplace")
						.setVisibleInFilterBar(true);
				}
				var oView = this.getView();
				/*	calling Utility controller,to set and load the Workplace and Technical Platz Data into JSON Model 
					for value help selection - deselection functionality by passing the selected plants as a filter.*/
				oTplaceJsonModel = Utility.setupTplaceModel(oDataModel, oView);
				oWplaceJsonModel = Utility.setupWplaceModel(oDataModel, oView);
			} else {
				if (oSelectedItems.length === 0) {
					this.getView()
						.byId("id_fg_workplace")
						.setVisibleInFilterBar(true);
				} else {
					this.getView()
						.byId("id_fg_workplace")
						.setVisibleInFilterBar(false);
				}
			}
		},
		//handle value help event code for work place
		handleValueHelpWorkPlace: function (oEvent) {
			var aInputWplace = [];
			this._valueHelpDialogWorkPlace = sap.ui.xmlfragment(
				"com.db.de.elan.util.WorkPlace",
				this
			);
			var aSelectedWplaces = this.getView()
				.byId("id_mi_workplace")
				.getTokens();
			for (var wplace = 0; wplace < aSelectedWplaces.length; wplace++) {
				aInputWplace.push(aSelectedWplaces[wplace].getText());
			}
			var aWplaces = this.getView()
				.getModel("oWplaceJsonModel")
				.getProperty("/WorkPlaceSet");
			for (var type = 0; type < aWplaces.length; type++) {
				aWplaces[type].selected = false;
			}
			for (var inputValue = 0; inputValue < aInputWplace.length; inputValue++) {
				for (var wplace = 0; wplace < aWplaces.length; wplace++) {
					if (aWplaces[wplace].Arbpl === aInputWplace[inputValue]) {
						aWplaces[wplace].selected = true;
					}
				}
				this.getView()
					.getModel("oWplaceJsonModel")
					.setProperty("/WorkPlaceSet", aWplaces);
			}
			this._valueHelpDialogWorkPlace.setMultiSelect(true);
			this._valueHelpDialogWorkPlace.setRememberSelections(true);
			this._valueHelpDialogWorkPlace.setShowClearButton(true);
			this.getView()
				.addDependent(this._valueHelpDialogWorkPlace);
			this._valueHelpDialogWorkPlace.open();
		},
		//handle value help search event for workplace
		handleValueHelpSearchWorkPlace: function (oEvent) {
			var sValue = oEvent.getParameter("value");
			var oFilter = new Filter("Arbpl", sap.ui.model.FilterOperator.Contains, sValue);
			var oBinding = oEvent.getSource()
				.getBinding("items");
			oBinding.filter([oFilter]);
		},
		//handle value help close event for workplace 
		handleValueHelpCloseWorkPlace: function (oEvent) {
			var oSelectedItems = oEvent.getParameter("selectedItems");
			this.byId("id_mi_workplace")
				.removeAllTokens();
			var aTitle = [];
			if (oSelectedItems) {
				for (var title = 0; title < oSelectedItems.length; title++) {
					var text = oSelectedItems[title].getTitle();
					aTitle.push(text);
				}
				for (var wplace = 0; wplace < aTitle.length; wplace++) {
					this.byId("id_mi_workplace")
						.addToken(new sap.m.Token({
							text: aTitle[wplace]
						}));
				}
			}
		},
		//handle value help wvent for Technical Platz
		handleValueHelpFL: function (oEvent) {
			var aInputTplace = [];
			this._valueHelpFuncLoc = sap.ui.xmlfragment(
				"com.db.de.elan.util.FunctionLocation",
				this
			);
			var aSelectedTplaces = this.getView()
				.byId("id_mi_tplace")
				.getTokens();
			for (var tplace = 0; tplace < aSelectedTplaces.length; tplace++) {
				aInputTplace.push(aSelectedTplaces[tplace].getText());
			}
			var aTplaces = this.getView()
				.getModel("oTplaceJsonModel")
				.getProperty("/TechnicalPlatzSet");
			for (var type = 0; type < aTplaces.length; type++) {
				aTplaces[type].selected = false;
			}
			for (var inputValue = 0; inputValue < aInputTplace.length; inputValue++) {
				for (var place = 0; place < aTplaces.length; place++) {
					if (aTplaces[place].Tplnr === aInputTplace[inputValue]) {
						aTplaces[place].selected = true;
					}
				}
				this.getView()
					.getModel("oTplaceJsonModel")
					.setProperty("/TechnicalPlatzSet", aTplaces);
			}
			this._valueHelpFuncLoc.setMultiSelect(true);
			this._valueHelpFuncLoc.setRememberSelections(true);
			this._valueHelpFuncLoc.setShowClearButton(true);
			this.getView()
				.addDependent(this._valueHelpFuncLoc);
			this._valueHelpFuncLoc.open();
		},
		//handle value help search event for Technical Platz
		handleValueHelpSearchFL: function (oEvent) {
			var sValue = oEvent.getParameter("value");
			var oFilter = new Filter("Tplnr", sap.ui.model.FilterOperator.Contains, sValue);
			var oBinding = oEvent.getSource()
				.getBinding("items");
			oBinding.filter([oFilter]);
		},
		//handle value help close event for Technical Platz 
		handleValueHelpCloseFL: function (oEvent) {
			var oSelectedItems = oEvent.getParameter("selectedItems");
			var aTitle = [];
			if (oSelectedItems) {
				this.byId("id_mi_tplace")
					.removeAllTokens();
				for (var title = 0; title < oSelectedItems.length; title++) {
					var text = oSelectedItems[title].getTitle();
					aTitle.push(text);
				}
				for (var tplace = 0; tplace < aTitle.length; tplace++) {
					this.byId("id_mi_tplace")
						.addToken(new sap.m.Token({
							text: aTitle[tplace]
						}));
				}
			}
		},
		/*Select event for variant management ui control. 
		Selecting the variant item from list will trigger this event*/
		onSelect: function (evt) {
			this.getView()
				.byId("id_variant")
				.currentVariantSetModified(true);
			var sVariantKey = evt.getParameters("key")
				.key;
			VariantManagement.onSelectVariant(sVariantKey, oView, oDataModel);
		},
		/*Manage event for variant management ui control. 
		Update and Deletion will trigger this event*/
		onManage: function (evt) {
			var sDefaultKeyString = evt.getParameters()
				.def;
			var aVariantName = [];
			var params = evt.getParameters();
			var deleted = params.deleted;
			//check for varaint deletion
			if (deleted.length > 0) {
				var deleted = params.deleted;
				for (var deleteItem = 0; deleteItem < deleted.length; deleteItem++) {
					var aValue = JSON.parse(deleted[deleteItem]);
					aVariantName.push(aValue[8]);
				}
				VariantManagement.onDeleteVariants(oView, aVariantName);
			} else {
				//calling the update operation for variant management
				VariantManagement.onUpdateVariant(evt, oView, oVariantDataModel);
			}
		},
		//onSave event will be triggered by Save/Save As option from the Screen by the User
		onSave: function (evt) {
			if ((oView.byId("id_mc_type")
					.getSelectedItems()
					.length < 0) || (oView.byId("id_mi_plant")
					.getTokens()
					.length < 0)) {
				MessageToast.show(oBundle.getText("master_variant_validation"));
			} else {
				VariantManagement.onSaveVariant(evt, oView, oBundle);
			}
		},
		//onClick Go Button Event
		onSearch: function (oEvent) {
			var bIsFromVariant = false
			var sVariantName = "";
			var aFilterArg = SelectionController.getSelection(oView, oBundle, bIsFromVariant, sVariantName); //getting all the selected values from the Screen either from Variant or Manual.
			aNotificationType = aFilterArg[0];
			aNotificationPriority = aFilterArg[1];
			aNotificationStatus = aFilterArg[2];
			dFromDate = aFilterArg[3];
			dToDate = aFilterArg[4];
			aPlant = aFilterArg[5];
			aFunctionalLocation = aFilterArg[6];
			aWorkPlace = aFilterArg[7];
			aRailObject = aFilterArg[9];
			oView
				.byId("id_panel_two")
				.setVisible(true);
			oView
				.byId("id_mc_type")
				.setValueState(sap.ui.core.ValueState.None);
			oView
				.byId("id_mi_plant")
				.setValueState(sap.ui.core.ValueState.None);
			GeoMap.setMapConfiguration(oView, "H");
			this.getFilters();
			oDataModel
				.attachRequestSent(function (evt) {
					oGlobalBusyDialog.open();
				});
			//setting the OData Model sizelimit as 1000 , to reducing the heavy load of data in the frontend
			oDataModel
				.setSizeLimit(1000);
			//applying all the filters in the Notifications Collections
			oView
				.byId("id_spots_collection")
				.getBinding("items")
				.filter(aGetNotificationFilters); //Applying filters to the EntitySet bounded with Maps
			oDataModel
				.attachRequestCompleted(function (evt) {
					oGlobalBusyDialog.close();
				});
			var bBoxValues = [];
			GeoServer.getGeoServerData(aRailObject, oView, bBoxValues);
			if (oView
				.byId("id_check_weather")
				.getSelected() === true) {
				WeatherService.getLatitudeLongitude(oDataModel, aGetNotificationFilters, oView);
			} else {
				WeatherService.getRemovWeather(oView);
			}
			oView
				.byId("id_panel_three")
				.setVisible(false);
			oView
				.byId("id_page")
				.setVisible(true);
			oView.byId("id_geomap")
				.setLegendVisible(false);
			oView.byId("id_tgle_map")
				.setText(this._getResourceBundle()
					.getText("satelite"));
			oView.byId("id_geomap")
				.setZoomlevel(8);
			oView.byId("id_semantic_page")
				.setHeaderExpanded(false);

		},
		/*getFilters method used to collect all the filter values from the selected flter. 
		All the selected filter values will be added in a single array and passed to the NotificationSet*/
		getFilters: function () {
			aGetNotificationFilters = []; //common array to get all the filters from the master screen and bind with NotificationsSet EntitySet
			if (aNotificationType) {
				for (var notiftype = 0; notiftype < aNotificationType.length; notiftype++) {
					aGetNotificationFilters.push(new Filter("Qmart", sap.ui.model.FilterOperator.EQ, aNotificationType[notiftype]));
				}
			}
			if (aNotificationPriority) {
				for (var notifpriority = 0; notifpriority < aNotificationPriority.length; notifpriority++) {
					aGetNotificationFilters.push(new Filter("Priok", sap.ui.model.FilterOperator.EQ, aNotificationPriority[notifpriority]));
				}
			}
			if (aNotificationStatus) {
				for (var notifstatus = 0; notifstatus < aNotificationStatus.length; notifstatus++) {
					aGetNotificationFilters.push(new Filter("Sttxt", sap.ui.model.FilterOperator.EQ, aNotificationStatus[notifstatus]));
				}
			}
			if (aPlant) {
				for (var plant = 0; plant < aPlant.length; plant++) {
					aGetNotificationFilters.push(new Filter("Iwerk", sap.ui.model.FilterOperator.EQ, aPlant[plant]));
				}
			}
			if (aFunctionalLocation) {
				for (var flocation = 0; flocation < aFunctionalLocation.length; flocation++) {
					aGetNotificationFilters.push(new Filter("Tplnr", sap.ui.model.FilterOperator.EQ, aFunctionalLocation[flocation]));
				}
			}
			if (aWorkPlace) {
				for (var workPlace = 0; workPlace < aWorkPlace.length; workPlace++) {
					aGetNotificationFilters.push(new Filter("Arbpl", sap.ui.model.FilterOperator.EQ, aWorkPlace[workPlace]));
				}
			}
			if (dFromDate && dToDate) {
				dFromDate.replace("-", "");
				dToDate.replace("-", "");
				aGetNotificationFilters.push(new Filter("Erdat", sap.ui.model.FilterOperator.BT, dFromDate, dToDate));
			}
			return aGetNotificationFilters;
		},
		onClickGroup: function () {
			this.getRouter()
				.navTo("object");
		},
		onSelectionToggle: function (evt) {
			var bTogglePressed = evt.getParameter("pressed");
			bRecZoomSelected = GeoMap.enableRectangularSelection(oView, bTogglePressed);
		},
		onZoomChanged: function (evt) {
			bRecZoomSelected = GeoMap.getBBOXSelection(oView, evt, aRailObject, bRecZoomSelected);
		},
		onPressHome: function () {
			bRecZoomSelected = GeoMap.setInitialHome(oView, aRailObject);
		},
		onClickChangeMap: function (evt) {
			GeoMap.setMapType(oView, oBundle, evt);
		},
		onPressLegend: function () {
			GeoMap.setLegend(oView);
		},
		onClickNotification: function (evt) {
			var oBindingContext = evt.getSource()
				.getBindingContext();
			this.getView()
				.byId("id_link_text")
				.setText(oBindingContext.getProperty("Qmnum"));
			this.getView()
				.byId("id_txt_qmtxt")
				.setText(oBindingContext.getProperty("Qmtxt"));
			this.getView()
				.byId("id_txt_qmlngtxt")
				.setHtmlText(oBindingContext.getProperty("Qmlongtxt"));
			this.getView()
				.byId("id_txt_tplnr")
				.setText(oBindingContext.getProperty("Tplnr"));
			this.getView()
				.byId("id_txt_pltxt")
				.setText(oBindingContext.getProperty("Pltxt"));
			switch (oBindingContext.getProperty("Priok")) {
			case "1":
				this.getView()
					.byId("id_txt_priority")
					.setText("1-Sehr hoch");
				break;
			case "2":
				this.getView()
					.byId("id_txt_priority")
					.setText("2-Hoch");
				break;
			case "3":
				this.getView()
					.byId("id_txt_priority")
					.setText("3-Mittel");
				break;
			case "4":
				this.getView()
					.byId("id_txt_priority")
					.setText("4-Niedrig");
				break;

			default:
			}
			var sText;
			if (oBindingContext.getProperty("Sttxt") === "MOFN") {
				sText = "Offen";
			} else if (oBindingContext.getProperty("Sttxt") === "MMAB") {
				sText = "Abgescholossen"
			}
			this.getView()
				.byId("id_txt_status")
				.setText(sText)
			var panel = this.getView()
				.byId("id_panel_three")
				.setVisible(true);
			this.getView()
				.byId("id_icon_tab_info")
				.setVisible(true);
			this.getView()
				.byId("id_icon_tab_weather")
				.setVisible(false);
			this.getView()
				.byId("id_icon_tab_forecast")
				.setVisible(false);

			this.getView()
				.byId("id_separator_two")
				.setVisible(false);
			this.getView()
				.byId("id_separator_one")
				.setVisible(false);
			this.getView()
				.byId("id_page")
				.scrollTo(800, 800); //Get Hold of Scroll Container

		},

		handleIconTabBarSelect: function (evt) {
			var sSelectedkey = evt.getParameter("key");

			if (sSelectedkey === "weather_actual") {
				WeatherService.getActualWeather(iLatitude, iLongitude, oView);
			} else if (sSelectedkey === "weather_forecast") {
				WeatherService.getForecastWeather(iLatitude, iLongitude, oView);
			}

		},
		onCreateGroup: function () {
			if (!this._oDialog) {
				this._oDialog = sap.ui.xmlfragment("com.db.de.elan.util.CreateGroupMaster", this);

			}
			sap.ui.getCore()
				.byId("id_ip_group_id_master")
				.setValue("");
			sap.ui.getCore()
				.byId("id_ip_group_type_master")
				.setValue("");
			sap.ui.getCore()
				.byId("id_ip_group_element_master")
				.setValue("");
			sap.ui.getCore()
				.byId("id_ip_description_master")
				.setValue("");
			sap.ui.getCore()
				.byId("id_dp_from_date_master")
				.setValue("");
			sap.ui.getCore()
				.byId("id_dp_to_date_master")
				.setValue("");
			var i18nModel = this.getOwnerComponent()
				.getModel("i18n");
			this._oDialog.setModel(i18nModel, "i18n");
			this._oDialog.open();
		},
		onCreateGroupCreation: function () {
			this._oDialog.close();

		},
		onCancelGroupCreation: function () {
			this._oDialog.close();

		},
		onClickSpot1: function (oEvent) {
			this.getView()
				.byId("id_page")
				.scrollTo(800, 800); //Get Hold of Scroll Container
			var oContextData = oEvent.getSource()
				.getBindingContext("weatherModel");
			this.getView()
				.byId("id_separator_one")
				.setVisible(false);
			this.getView()
				.byId("id_separator_two")
				.setVisible(true);
			iLatitude = oContextData.getProperty("lattitude");
			iLongitude = oContextData.getProperty("longitude");
			var panel = this.getView()
				.byId("id_panel_three")
				.setVisible(true);
			this.getView()
				.byId("id_icon_tab_info")
				.setVisible(false);
			this.getView()
				.byId("id_icon_tab_weather")
				.setVisible(true);
			this.getView()
				.byId("id_icon_tab_forecast")
				.setVisible(true);
			var oView = this.getView();
			WeatherService.getActualWeather(iLatitude, iLongitude, oView);

		},
		/*	Cross Application Navigation method to call Notification IW23 Transacrion in SAP Web GUI. 
		it will carry the currrent notification number as key to IW23 Transaction.*/
		onCrossAppNavigation: function () {
			var notificationNumber = this.getView()
				.byId("id_link_text")
				.getText();
			if (sap.ushell.Container.getService("CrossApplicationNavigation")) {
				sap.ushell.Container.getService("CrossApplicationNavigation")
					.toExternal({
						target: {
							shellHash: "#Z_SPI_TC_IW23_SEM-display?RIWO00-QMNUM=" + notificationNumber
						}
					});
			}
		},
		/*Calling the TP Cockpit app from Meldungradar App via FLP - Cross Application navigation
		After initializing the ushell services, provide the semantic object and action name of TP Cockpit Application.onTpcCall
		As a Parameter, Technical Platz ID is passed.
		sap.m.URLHelper used to open the target application in the new tab .*/
		onTpcCall: function () {
			this.getView()
				.byId("id_scroll_container")
				.scrollTo(0, 0);
			var sTechnicalPlatz = this.getView()
				.byId("id_txt_tplnr")
				.getText();
			var crossAppNav = sap.ushell.Container.getService("CrossApplicationNavigation");
			var kbltHash = (crossAppNav && crossAppNav.hrefForExternal({
				target: {
					semanticObject: "Z_SPI_KBLT_SEM",
					action: "display"
				},
				params: {
					"sTechnicalPlatz": sTechnicalPlatz
				}
			})) || "";
			sap.m.URLHelper.redirect(window.location.href.split('#')[0] + kbltHash, true);
		},
		//onclick for hectometer
		onHectoMeterItemClick: function (evt) {
			detailContent = GisPopover.getHectometerPopover(evt);
			evt.getSource()
				.openDetailWindow("HektoMeter");
		},
		onCrossingItemClick: function (evt) {
			detailContent = GisPopover.getCrossingPopover(evt);
			evt.getSource()
				.openDetailWindow("BAHNUEBERGAENGE");
		},
		onClickSignalItem: function (evt) {
			detailContent = GisPopover.getSignalPopover(evt);
			evt.getSource()
				.openDetailWindow("Signale");
		},
		onClickSmallBridgeItem: function (evt) {
			detailContent = GisPopover.getSmallBridgePopover(evt);
			evt.getSource()
				.openDetailWindow("GEO_BRUECKEN_KL");
		},
		onClickBBridgeItem: function (evt) {
			detailContent = GisPopover.getBigbridgePopover(evt);
			evt.getSource()
				.openDetailWindow("GEO_BRUECKEN_GR");
		},
		onClickWallItem: function (evt) {
			detailContent = GisPopover.getWallPopover(evt);
			evt.getSource()
				.openDetailWindow("SCHUTZWAND_LINIE");
		},
		onClickLockItem: function (evt) {
			detailContent = GisPopover.getTracklockPopover(evt);
			evt.getSource()
				.openDetailWindow("GLEISSPERREN");
		},
		onClickIndusisItem: function (evt) {
			detailContent = GisPopover.getIndusisPopover(evt);
			evt.getSource()
				.openDetailWindow("INDUSI");
		},
		onClickTurnItem: function (evt) {
			detailContent = GisPopover.getTurnoutPopover(evt);
			evt.getSource()
				.openDetailWindow("WEICHE_KREUZUNG");
		},
		onCloseDetail: function (evt) {},
		onOpenDetail: function (evt) {
			GisPopover.openDetail(evt, detailContent);
		}

	});

});