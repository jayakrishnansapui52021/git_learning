sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	"sap/ui/model/json/JSONModel"
], function (Controller, History, JSONModel) {
	"use strict";

	return Controller.extend("com.db.de.elan.controller.BaseController", {
		/**
		 * Convenience method for accessing the router in every controller of the application.
		 * @public
		 * @returns {sap.ui.core.routing.Router} the router for this component
		 */
		getRouter: function () {
			return this.getOwnerComponent()
				.getRouter();
		},

		/**
		 * Convenience method for getting the view model by name in every controller of the application.
		 * @public
		 * @param {string} sName the model name
		 * @returns {sap.ui.model.Model} the model instance
		 */
		getModel: function (sName) {
			return this.getView()
				.getModel(sName);
		},
		getLocalModel: function () {
			var oNotificationTypeModel = new JSONModel(jQuery.sap.getModulePath("com.db.de.elan",
				"/localService/mockdata/NotificationType.json"));
			this.getOwnerComponent()
				.setModel(oNotificationTypeModel, "NotificationType");
			var oPriorityModel = new JSONModel(jQuery.sap.getModulePath("com.db.de.elan", "/localService/mockdata/Priority.json"));
			this.getOwnerComponent()
				.setModel(oPriorityModel, "Priority");
			var oGisRailModel = new JSONModel(jQuery.sap.getModulePath("com.db.de.elan", "/localService/mockdata/RailObjects.json"));
			this.getOwnerComponent()
				.setModel(oGisRailModel, "RailObjects");
		},

		/**
		 * Convenience method for setting the view model in every controller of the application.
		 * @public
		 * @param {sap.ui.model.Model} oModel the model instance
		 * @param {string} sName the model name
		 * @returns {sap.ui.mvc.View} the view instance
		 */
		setModel: function (oModel, sName) {
			return this.getView()
				.setModel(oModel, sName);
		},

		/**
		 * Convenience method for getting the resource bundle.
		 * @public
		 * @returns {sap.ui.model.resource.ResourceModel} the resourceModel of the component
		 */
		getResourceBundle: function () {
			return this.getOwnerComponent()
				.getModel("i18n")
				.getResourceBundle();
		},

		/**
		 * Event handler for navigating back.
		 * It there is a history entry we go one step back in the browser history
		 * If not, it will replace the current entry of the browser history with the master route.
		 * @public
		 */
		onNavBack: function () {
			var sPreviousHash = History.getInstance()
				.getPreviousHash();

			if (sPreviousHash !== undefined) {
				// eslint-disable-next-line sap-no-history-manipulation
				history.go(-1);
			} else {
				this.getRouter()
					.navTo("master", {}, true);
			}
		}

	});

});