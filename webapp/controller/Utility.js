sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"sap/ui/model/Filter",
	"sap/ui/model/json/JSONModel"
], function (Controller, MessageToast, Filter, JSONModel) {
	"use strict";
	var oGlobalBusyDialog;
	var oGlobalBusyDialog1;
	var aTplnrFilter;
	var aWorkPlaceFilter;
	return {

		/*keeping the Plant model locally for the application functionality. 
		For the Plant,Workplace and TechnicalPlatz Selection , we used standard dialog with multi selection.
		When user selects multiple data from the List, it should appear on the input control with close option enabled.
		when user close the seelction, it should refelct inside the list too. 
		To enable and acheive this custom functionality we maintained all three models locally on JSON in runtime.*/
		setupPlantModel: function (oModel, oView) {
			var oPlantJsonModel = new JSONModel(); //local json model initializing
			oModel.read("/PlantSet", {
				success: function (oEvent) {
					var aPlantList = [];
					$.each(oEvent.results, function (data, item) {
						aPlantList.push({
							"Key": item.Iwerk,
							"Name": item.Name1,
							"selected": false
						}); //pushing the Iwerk,Name1 and selected of PlantSet into array aPlantList.
					});
					var jsonModel = new JSONModel({
						"PlantSet": aPlantList //pushing aPlantList array into JSON Model
					});
					var plantData = jsonModel.getData();
					oPlantJsonModel.setData(plantData);
					oView.setModel(oPlantJsonModel, "oPlantJsonModel");
					return oPlantJsonModel;
				},
				error: function (oError) {
					jQuery.sap.log.info("Error failed at PlantSet " + oError);
				}
			});
		},
		//setupFilter function to make the filter collection for the workplace and technical platz
		setupFilter: function (oView) {
			aTplnrFilter = [];
			aWorkPlaceFilter = [];
			var aPlant = [];
			var aSelectedPlants = oView.byId("id_mi_plant")
				.getTokens();
			for (var plant = 0; plant < aSelectedPlants.length; plant++) {
				aPlant.push(aSelectedPlants[plant].getText());
			}
			for (var plant = 0; plant < aPlant.length; plant++) {
				aTplnrFilter.push(new Filter(
					"Swerk",
					sap.ui.model.FilterOperator.EQ, aPlant[plant]
				))
				aWorkPlaceFilter.push(new Filter(
					"Werks",
					sap.ui.model.FilterOperator.EQ, aPlant[plant]
				))
			}
		},
		/*keeping the Technical Platz model locally for the application functionality. 
			For the Plant,Workplace and TechnicalPlatz Selection , we used standard dialog with multi selection.
			When user selects multiple data from the List, it should appear on the input control with close option enabled.
			when user close the seelction, it should refelct inside the list too. 
			To enable and acheive this custom functionality we maintained all three models locally on JSON in runtime.*/
		setupTplaceModel: function (oModel, oView) {
			this.setupFilter(oView);
			var oTplaceJsonModel = new JSONModel(); //local json model initializing
			oGlobalBusyDialog1 = new sap.m.BusyDialog();
			//	oGlobalBusyDialog1.open();
			oModel.read("/TechnicalPlatzSet", {
				filters: aTplnrFilter,
				success: function (oEvent) {
					var aTPlaceList = [];
					$.each(oEvent.results, function (data, item) {
						aTPlaceList.push({
							"Tplnr": item.Tplnr,
							"Pltxt": item.Pltxt,
							"Swerk": item.Swerk,
							"selected": false
						}); //pushing the Tplnr,Swerk,Pltxt and selected of TechnicalPlatzSet into array aTPlaceList.
					});
					var jsonModel = new JSONModel({
						"TechnicalPlatzSet": aTPlaceList //pushing TechnicalPlatzSet array into JSON Model
					});
					var tplaceData = jsonModel.getData();
					oTplaceJsonModel.setData(tplaceData);
					oView.setModel(oTplaceJsonModel, "oTplaceJsonModel");
					//oGlobalBusyDialog1.close();
					return oTplaceJsonModel;
				},
				error: function (oError) {
					//	oGlobalBusyDialog1.close();
					jQuery.sap.log.info("Error failed at PlantSet " + oError);
				}
			});
		},
		/*keeping the Workplace model locally for the application functionality. 
			For the Plant,Workplace and TechnicalPlatz Selection , we used standard dialog with multi selection.
			When user selects multiple data from the List, it should appear on the input control with close option enabled.
			when user close the seelction, it should refelct inside the list too. 
			To enable and acheive this custom functionality we maintained all three models locally on JSON in runtime.*/
		setupWplaceModel: function (oModel, oView) {
			this.setupFilter(oView);
			var oWplaceJsonModel = new JSONModel(); //local json model initializing
			oModel.read("/WorkPlaceSet", {
				filters: aWorkPlaceFilter,
				success: function (oEvent) {
					var aWPlaceList = [];
					$.each(oEvent.results, function (data, item) {
						aWPlaceList.push({
							"Arbpl": item.Arbpl,
							"Werks": item.Werks,
							"selected": false
						}); //pushing the Arbpl,Werks and selected of WorkPlaceSet into array aWPlaceList.
					});
					var jsonModel = new JSONModel({
						"WorkPlaceSet": aWPlaceList //pushing aWPlaceList array into JSON Model
					});
					var wplaceData = jsonModel.getData();
					oWplaceJsonModel.setData(wplaceData);
					oView.setModel(oWplaceJsonModel, "oWplaceJsonModel");
					return oWplaceJsonModel;
				},
				error: function (oError) {
					jQuery.sap.log.info("Error failed at PlantSet " + oError);
				}
			});
			return oWplaceJsonModel;
		},
		getDateFormatter: function (bIsFromVariant, dDate) {
			var sPattern;
			if (bIsFromVariant === true) {
				sPattern = "yyyy.MM.dd"
			} else {
				sPattern = "yyyyMMdd"
			}
			var oDateFormatterMonthWithDate = sap.ui.core.format.DateFormat.getInstance({
				pattern: sPattern
			});
			var dFormattedDate = oDateFormatterMonthWithDate.format(dDate);

			return dFormattedDate;
		},
		clearValues: function (oView) {
			oView.byId("id_mc_type")
				.removeAllSelectedItems();
			oView.byId("id_mc_priority")
				.removeAllSelectedItems();
			oView.byId("id_mc_status")
				.removeAllSelectedItems();
			oView.byId("id_mi_plant")
				.removeAllTokens();
			oView.byId("id_mi_workplace")
				.removeAllTokens();
			oView.byId("id_mi_tplace")
				.removeAllTokens();
			oView.byId("id_dr_date")
				.setValue("");
			oView.byId("id_mc_rail_object")
				.removeAllSelectedItems();
			oView.byId("id_check_weather")
				.setSelected(false);
		}

	};
});