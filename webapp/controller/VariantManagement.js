sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"./SelectionController",
	"sap/m/MessageToast",
	"sap/ui/model/Filter",
	"./Utility",
	"./ServiceControl"
], function (Controller, SelectionController, MessageToast, Filter, Utility, ServiceControl) {
	"use strict";
	var sVariantName;
	var oGlobalBusyDialog;
	var oGlobalBusyDialog1;
	var sVariantKeyUpdate;
	var aSelectedValues;
	var aVar_NotificationType;
	var aVar_NotificationPriority;
	var aVar_NotificationStatus;
	var aVar_NotifiactionDateFrom;
	var aVar_NotificationDateTo;
	var aVar_Plant;
	var aVar_WorkPlace;
	var aVar_FunctionalLocation;
	var aVar_RailObjects;
	var aVar_Weather;
	return {
		/**
		 * VariantManagement.js is a Common Javascipt controller for Variant Management functioanlity
		 * It is having following functions to handle Save,Delete and Update the Variant Informations
		 */
		//Save Variant function to handle the Create Variant Operation.
		onSaveVariant: function (evt, oView, oBundle) {
			var bIsfromVariant = true; //booblean variable to identify call happeing from Variant or not.
			var bDefault = evt.getParameters("name")
				.def;
			sVariantName = evt.getParameters("name")
				.name;
			var aFilterData = SelectionController.getSelection(oView, oBundle, bIsfromVariant, sVariantName);
			var sDataString = JSON.stringify(aFilterData);

			ServiceControl.getServiceData(oView, "C", false, sVariantName, sDataString, bDefault);

		},
		_getSelectedVariantData: function (sVariantKey) {
			aSelectedValues = JSON.parse(sVariantKey);
			aVar_NotificationType = [];
			aVar_NotificationPriority = [];
			aVar_NotificationStatus = [];
			aVar_NotifiactionDateFrom = [];
			aVar_NotificationDateTo = [];
			aVar_Plant = [];
			aVar_WorkPlace = [];
			aVar_FunctionalLocation = [];
			aVar_RailObjects = [];
			aVar_Weather = [];
			aVar_NotificationType.push(aSelectedValues[0]);
			aVar_NotificationPriority.push(aSelectedValues[1]);
			aVar_NotificationStatus.push(aSelectedValues[2]);
			aVar_NotifiactionDateFrom.push(aSelectedValues[3]);
			aVar_NotificationDateTo.push(aSelectedValues[4]);
			aVar_Plant.push(aSelectedValues[5]);
			aVar_WorkPlace.push(aSelectedValues[6]);
			aVar_FunctionalLocation.push(aSelectedValues[7]);
			aVar_RailObjects.push(aSelectedValues[9]);
			aVar_Weather.push(aSelectedValues[10]);
		},
		_setSelectedVariants: function (oView) {
			//reading the selected values from notification type array,if available. setting the selected values on the input
			if (aVar_NotificationType[0] !== null) {
				var aVarType = [];
				for (var type = 0; type < aVar_NotificationType[0].length; type++) {
					aVarType.push(aVar_NotificationType[0][type]);
				}
				oView.byId("id_mc_type")
					.setSelectedKeys(aVarType);
			}
			//reading the selected values from notification priority array,if available. setting the selected values on the input
			if (aVar_NotificationPriority[0] !== null) {
				var aVarPriority = [];
				for (var priority = 0; priority < aVar_NotificationPriority[0].length; priority++) {
					aVarPriority.push(aVar_NotificationPriority[0][priority]);
				}
				oView.byId("id_mc_priority")
					.setSelectedKeys(aVarPriority);
			}
			//reading the selected values from notification status array,if available. setting the selected values on the input
			if (aVar_NotificationStatus[0] !== null) {
				var aVarStatus = [];
				for (var status = 0; status < aVar_NotificationStatus[0].length; status++) {
					aVarStatus.push(aVar_NotificationStatus[0][status]);
				}
				oView.byId("id_mc_status")
					.setSelectedKeys(aVarStatus);
			}
			//reading the selected plants,if available. setting the selected values on the input.
			if (aVar_Plant[0] !== null) {
				var aVarPlant = [];
				for (var plant = 0; plant < aVar_Plant[0].length; plant++) {
					aVarPlant.push(aVar_Plant[0][plant]);
				}
				for (var plant = 0; plant < aVarPlant.length; plant++) {
					oView.byId("id_mi_plant")
						.addToken(new sap.m.Token({
							text: aVarPlant[plant]
						}));
				}
				oView.byId("id_fg_workplace")
					.setVisibleInFilterBar(true);
			} else {
				oView.byId("id_fg_workplace")
					.setVisibleInFilterBar(false);
			}
			//reading the selected workplace,if available. setting the selected values on the input.
			if (aVar_WorkPlace[0] !== null) {
				var aVarWorkPlace = [];
				for (var wplace = 0; wplace < aVar_WorkPlace[0].length; wplace++) {
					aVarWorkPlace.push(aVar_WorkPlace[0][wplace]);
				}
				for (var wplace = 0; wplace < aVarWorkPlace.length; wplace++) {
					oView.byId("id_mi_workplace")
						.addToken(new sap.m.Token({
							text: aVarWorkPlace[wplace]
						}));
				}
			}
			//reading the selected functional locations,if available. setting the selected values on the input.
			if (aVar_FunctionalLocation[0] !== null) {
				var aTplace = [];
				for (var tplace = 0; tplace < aVar_FunctionalLocation[0].length; tplace++) {
					aTplace.push(aVar_FunctionalLocation[0][tplace]);
				}
				for (var tplace = 0; tplace < aTplace.length; tplace++) {
					oView.byId("id_mi_tplace")
						.addToken(new sap.m.Token({
							text: aTplace[tplace]
						}));
				}
			}
			//reading the selected functional locations,if available. setting the selected values on the input.
			if (aVar_RailObjects[0] !== null) {
				var aRailObject = [];
				for (var railobj = 0; railobj < aVar_RailObjects[0].length; railobj++) {
					aRailObject.push(aVar_RailObjects[0][railobj]);
				}
				oView.byId("id_mc_rail_object")
					.setSelectedKeys(aRailObject);
			}
			// 		if (aVar_NotificationPriority[0] !== null) {
			// 	var aVarPriority = [];
			// 	for (var priority = 0; priority < aVar_NotificationPriority[0].length; priority++) {
			// 		aVarPriority.push(aVar_NotificationPriority[0][priority]);
			// 	}
			// 	oView.byId("id_mc_priority").setSelectedKeys(aVarPriority);
			// }

			if (aVar_NotifiactionDateFrom[0] === "") {
				oView.byId("id_dr_date")
					.setValue("");
			} else {
				var dFromDate = new Date(aVar_NotifiactionDateFrom[0]);
				var dToDate = new Date(aVar_NotificationDateTo[0]);
				oView.byId("id_dr_date")
					.setDateValue(dFromDate);
				oView.byId("id_dr_date")
					.setSecondDateValue(dToDate);
			}
			if (aVar_Weather[0] === true) {

				oView.byId("id_check_weather")
					.setSelected(true);
			} else {
				oView.byId("id_check_weather")
					.setSelected(false);
			}
		},

		//OnSelction event called,when user seelct the variant from the list.
		onSelectVariant: function (sVariantKey, oView, oDataModel) {
			//If it is Standard variant , remove all the seelctions
			if (sVariantKey === "*standard*" || sVariantKey === "") {
				Utility.clearValues(oView);
				oView.byId("id_fg_workplace")
					.setVisibleInFilterBar(false);
			} else {
				this._getSelectedVariantData(sVariantKey);
				Utility.clearValues(oView);
				this._setSelectedVariants(oView);

				var oTplaceJsonModel = Utility.setupTplaceModel(oDataModel, oView);
				var oWplaceJsonModel = Utility.setupWplaceModel(oDataModel, oView);
				oView.setModel(oTplaceJsonModel, "oTplaceJsonModel");
				oView.setModel(oWplaceJsonModel, "oWplaceJsonModel");
			}
		},
		//updateVariant function used to update the variant data. In this case it is mainly used to update the default variant selection
		onUpdateVariant: function (evt, oView, oVariantDataModel) {
			//check for update variant as a standard or custom
			if (evt.getParameters()
				.def === "*standard*") {
				var aVariantName;
				var variantFilter = new Filter(
					"Applid",
					sap.ui.model.FilterOperator.EQ, "ZMAI_UI5_ELAN"
				);
				/*if update required for standard variant, check the standard variant already exists.
				if standard variant is not exists in backend, create it as a new variant namely Standard and key as *standard*.
				if already exists, update the required status as default as true */
				oVariantDataModel.read("/VariantsSet", {
					filters: [variantFilter], //passing the filter parameters
					success: function (oEvent) {
						aVariantName = [];
						$.each(oEvent.results, function (data, item) {
							//pushing the variant name into aVariantName array for checking standard variant
							aVariantName.push(item.Variant);
						});
						if (aVariantName.includes("Standard")) {

							ServiceControl.getServiceData(oView, "U", true, "Standard", "*standard*", true);

						} else {
							ServiceControl.getServiceData(oView, "C", true, "Standard", "*standard*", bDefault);

						}
					},
					error: function () {
						oGlobalBusyDialog.close();
					}
				});
			} else {

				var bIsfromVariant = true;
				var sDefaultkey = evt.getParameters()
					.def;
				sVariantKeyUpdate = evt.getParameters()
					.def;
				var aDefaultKeys = JSON.parse(sDefaultkey);
				var sVariantName = aDefaultKeys[8];
				var sDataString = JSON.stringify(JSON.parse(sDefaultkey));

				ServiceControl.getServiceData(oView, "U", true, sVariantName, sDataString, true);

			}
		},
		onDeleteVariants: function (oView, aDeleteVariants) {
			ServiceControl.getServiceData(oView, "D", true, aDeleteVariants, "", true);
		}

	};
});