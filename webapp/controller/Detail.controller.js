sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"../model/formatter",
	"sap/ui/model/Filter",
	"sap/m/library",
	"sap/m/MessageToast",
	"./TableControl"

], function (BaseController, JSONModel, formatter, Filter, mobileLibrary, MessageToast, TableControl) {
	"use strict";

	// shortcut for sap.m.URLHelper
	var URLHelper = mobileLibrary.URLHelper;
	var aSelectedGroupElementContext;

	return BaseController.extend("com.db.de.elan.controller.Detail", {

		formatter: formatter,

		/* =========================================================== */
		/* lifecycle methods                                           */
		/* =========================================================== */

		onInit: function () {
			// Model used to manipulate control states. The chosen values make sure,
			// detail page is busy indication immediately so there is no break in
			// between the busy indication for loading the view's meta data
			var oViewModel = new JSONModel({
				busy: false,
				delay: 0

			});

			this.getRouter()
				.getRoute("object")
				.attachPatternMatched(this._onObjectMatched, this);
			this.setModel(oViewModel, "detailView");

		},

		/* =========================================================== */
		/* begin: internal methods                                     */
		/* =========================================================== */

		/**
		 * Binds the view to the object path and expands the aggregated line items.
		 * @function
		 * @param {sap.ui.base.Event} oEvent pattern match event in route 'object'
		 * @private
		 */
		_onObjectMatched: function (oEvent) {

			this.getModel("appView")
				.setProperty("/layout", "TwoColumnsMidExpanded");

			var oModel = new JSONModel(sap.ui.require.toUrl("com/db/de/elan//localService/mockdata") + "/SalesOrder.json");
			this.setModel(oModel, "SalesOrder");

		},
		//user defined events
		//getting the selected values from group table
		onSelectionChangeGroup: function (evt) {
			debugger;
			var aSelectedGroupId = [];

			var oGroupElementTable = this.getView()
				.byId("id_table_groupElement");
			var oGroupTable = this.getView()
				.byId("id_table_group");
			var aSelectedIndices = evt.getSource()
				.getSelectedIndices();
			for (var index = 0; index < aSelectedIndices.length; index++) {
				aSelectedGroupId.push(oGroupTable.getContextByIndex(aSelectedIndices[index])
					.getProperty("GroupId"))
			}
			oGroupElementTable.removeAllColumns();
			var oBundle = this.getResourceBundle();
			TableControl.setupColumn(oGroupElementTable, oBundle);
			var columnListItem = TableControl.setupTableColumnItem();
			for (var element = 0; element < aSelectedGroupId.length; element++) {
				var oGroupElementTableBindingInfo = {
					path: "elan>/GroupSet(guid'" + aSelectedGroupId[element] + "')/Group_ElementSet",
					template: columnListItem

				};
				oGroupElementTable.bindAggregation("items", oGroupElementTableBindingInfo);
			}
			//calling GroupElementSet

			// var oRGlobalBusyDialog = new sap.m.BusyDialog();
			// oRGlobalBusyDialog.open();
			// var oView = this.getView();
			// var that = this;
			// var oElanModel = this.getView()
			// 	.getModel("elan");
			// oElanModel.read("/Group_ElementSet", {

			// 	success: function (oEvent) {
			// 		var aGroupElement = [];

			// 		$.each(oEvent.results, function (data, item) {
			// 			//	if (item.Filterstring !== "") {
			// 			aGroupElement.push({
			// 				"GroupId": item.GroupId,
			// 				"GroupingElementid": item.GroupingElementid,
			// 				"Description": item.Description,
			// 				"Startts": item.Startts,
			// 				"Endets": item.Endets,
			// 				"Seqno": item.Seqno
			// 			}); //pushing the Filterstring and Is_Standard of VariantsSet into array aVariantKey.
			// 			//	}
			// 		});
			// 		//setting the data into local model
			// 		var jsonModel = new JSONModel({
			// 			"GroupElementSet": aGroupElement //pushing aGroupElement array into JSON Model
			// 		});
			// 		var oGroupElementJsonModel = new JSONModel();
			// 		var groupElementData = jsonModel.getData();
			// 		oGroupElementJsonModel.setData(groupElementData);
			// 		that
			// 			.setModel(oGroupElementJsonModel, "oGroupElementJson");

			// 		oRGlobalBusyDialog.close();
			// 		console.log(aGroupElement.length);
			// 		var aIndex = [];

			// 		for (var index = 0; index < aSelectedGroupId.length; index++) {
			// 			for (var selectedIndex = 0; selectedIndex < aGroupElement.length; selectedIndex++) {
			// 				if (aSelectedGroupId[index] === aGroupElement[selectedIndex].GroupId) {
			// 					aIndex.push(selectedIndex);
			// 				}
			// 			}
			// 		}
			// 		var iCount = oGroupElementTable._getTotalRowCount();
			// 		//You can't use getRows(), cuz it only returns the current rows on screen

			// 		//Now we need the data of every row

			// 		// for (var i = 0; i < iCount; i++) {

			// 		// 	var oEntity = oGroupElementTable.getModel()
			// 		// 		.getData(oTable.getContextByIndex(i)
			// 		// 			.getPath());

			// 		// 	if (oEntity.Name === "Whatever") {

			// 		// 		oTable.setSelectedIndex(i);
			// 		// 	}
			// 		// }

			// 		// oGroupElementTable.setSelectionInterval(0, 0);
			// 		// oGroupElementTable.setSelectionInterval(1, 1);
			// 		// if (aIndex.length > 0) {
			// 		// 	for (var index = 0; index < aIndex.length; index++) {
			// 		// 		oGroupElementTable.setSelectionInterval(aIndex[index], aIndex[index]);
			// 		// 	}
			// 		// } else {
			// 		// 	oGroupElementTable.setSelectedIndex(-1);
			// 		// }
			// 		// oGroupElementTable.fireRowSelectionChange({
			// 		// 	rowIndices: aIndex
			// 		// });

			// 		//this.selectOrdersBy("GroupId", "EQ", aSelectedGroupId[0]);

			// 	},
			// 	error: function () {
			// 		oRGlobalBusyDialog.close();
			// 	}
			// });

		},
		onCreateGroup: function () {
			if (!this._oDialog) {
				this._oDialog = sap.ui.xmlfragment("com.db.de.elan.util.CreateGroup", this);
			}
			sap.ui.getCore()
				.byId("id_ip_group_id")
				.setValue("");
			sap.ui.getCore()
				.byId("id_ip_group_type")
				.setValue("");
			sap.ui.getCore()
				.byId("id_ip_group_element")
				.setValue("");
			sap.ui.getCore()
				.byId("id_ip_description")
				.setValue("");
			sap.ui.getCore()
				.byId("id_dp_from_date")
				.setValue("");
			sap.ui.getCore()
				.byId("id_dp_to_date")
				.setValue("");
			var i18nModel = this.getOwnerComponent()
				.getModel("i18n");
			this._oDialog.setModel(i18nModel, "i18n");
			this._oDialog.open();
		},
		onCreateGroupCreation: function () {
			this._oDialog.close();
		
		},
		onCancelGroupCreation: function () {
			this._oDialog.close();
			
		},
		onSelectionChangeGroupElement: function (evt) {
			aSelectedGroupElementContext = [];
			var aSelectedIndices = evt.getSource()
				.getSelectedIndices();
			var oGroupElementTable = this.getView()
				.byId("id_table_group_element");
			for (var index = 0; index < aSelectedIndices.length; index++) {
				aSelectedGroupElementContext.push(oGroupElementTable.getContextByIndex(aSelectedIndices[index]));
			}

			//setting the data into local model
			var jsonModel = new JSONModel({
				"GroupElementContextSet": aSelectedGroupElementContext //pushing aSelectedGroupElementContext array into JSON Model
			});
			var oGroupElementContextJsonModel = new JSONModel();
			var groupElementContextData = jsonModel.getData();
			oGroupElementContextJsonModel.setData(groupElementContextData);
			sap.ui.getCore()
				.setModel(oGroupElementContextJsonModel, "oGroupElementContextJson");
			this
				.setModel(oGroupElementContextJsonModel, "oGroupElementContextJson");
		},
		//onPressEdit event will be triggered when user select the groupelemt.
		//the SelectedGroupEleements will be opened in the MultiInstancelayout
		onPressEdit: function () {

			this.getRouter()
				.navTo("multiinstance");
		},

		/**
		 * Binds the view to the object path. Makes sure that detail view displays
		 * a busy indicator while data for the corresponding element binding is loaded.
		 * @function
		 * @param {string} sObjectPath path to the object to be bound to the view.
		 * @private
		 */
		_bindView: function (sObjectPath) {
			// Set busy indicator during view binding
			var oViewModel = this.getModel("detailView");

			// If the view was not bound yet its not busy, only if the binding requests data it is set to busy again
			oViewModel.setProperty("/busy", false);

			this.getView()
				.bindElement({
					path: sObjectPath,
					events: {
						change: this._onBindingChange.bind(this),
						dataRequested: function () {
							oViewModel.setProperty("/busy", true);
						},
						dataReceived: function () {
							oViewModel.setProperty("/busy", false);
						}
					}
				});
		},

		_onBindingChange: function () {
			var oView = this.getView(),
				oElementBinding = oView.getElementBinding();

			// No data for the binding
			if (!oElementBinding.getBoundContext()) {
				this.getRouter()
					.getTargets()
					.display("detailObjectNotFound");
				// if object could not be found, the selection in the master list
				// does not make sense anymore.
				this.getOwnerComponent()
					.oListSelector.clearMasterListSelection();
				return;
			}

			var sPath = oElementBinding.getPath(),
				oResourceBundle = this.getResourceBundle(),
				oObject = oView.getModel()
				.getObject(sPath),
				sObjectId = oObject.ObjectID,
				sObjectName = oObject.Name,
				oViewModel = this.getModel("detailView");

			this.getOwnerComponent()
				.oListSelector.selectAListItem(sPath);

			oViewModel.setProperty("/shareSendEmailSubject",
				oResourceBundle.getText("shareSendEmailObjectSubject", [sObjectId]));
			oViewModel.setProperty("/shareSendEmailMessage",
				oResourceBundle.getText("shareSendEmailObjectMessage", [sObjectName, sObjectId, location.href]));
		},

		_onMetadataLoaded: function () {
			// Store original busy indicator delay for the detail view
			var iOriginalViewBusyDelay = this.getView()
				.getBusyIndicatorDelay(),
				oViewModel = this.getModel("detailView"),
				oLineItemTable = this.byId("lineItemsList"),
				iOriginalLineItemTableBusyDelay = oLineItemTable.getBusyIndicatorDelay();

			// Make sure busy indicator is displayed immediately when
			// detail view is displayed for the first time
			oViewModel.setProperty("/delay", 0);
			oViewModel.setProperty("/lineItemTableDelay", 0);

			oLineItemTable.attachEventOnce("updateFinished", function () {
				// Restore original busy indicator delay for line item table
				oViewModel.setProperty("/lineItemTableDelay", iOriginalLineItemTableBusyDelay);
			});

			// Binding the view will set it to not busy - so the view is always busy if it is not bound
			oViewModel.setProperty("/busy", true);
			// Restore original busy indicator delay for the detail view
			oViewModel.setProperty("/delay", iOriginalViewBusyDelay);
		},

		/**
		 * Set the full screen mode to false and navigate to master page
		 */
		onCloseDetailPress: function () {
			this.getModel("appView")
				.setProperty("/actionButtonsInfo/midColumn/fullScreen", false);
			// No item should be selected on master after detail page is closed
			// this.getOwnerComponent()
			// 	.oListSelector.clearMasterListSelection();
			this.getRouter()
				.navTo("master");
		},

		/**
		 * Toggle between full and non full screen mode.
		 */
		toggleFullScreen: function () {
			var bFullScreen = this.getModel("appView")
				.getProperty("/actionButtonsInfo/midColumn/fullScreen");
			this.getModel("appView")
				.setProperty("/actionButtonsInfo/midColumn/fullScreen", !bFullScreen);
			if (!bFullScreen) {
				// store current layout and go full screen
				this.getModel("appView")
					.setProperty("/previousLayout", this.getModel("appView")
						.getProperty("/layout"));
				this.getModel("appView")
					.setProperty("/layout", "MidColumnFullScreen");
			} else {
				// reset to previous layout
				this.getModel("appView")
					.setProperty("/layout", this.getModel("appView")
						.getProperty("/previousLayout"));
			}
		}
	});

});