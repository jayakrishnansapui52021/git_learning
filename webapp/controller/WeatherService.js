sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"sap/ui/model/Filter",
	"sap/ui/model/json/JSONModel"
], function (Controller, MessageToast, Filter, JSONModel) {
	"use strict";
	var oGlobalBusyDialog;
	var aLatLongCollection;
	var aWeatherCollection;
	var oGlobalBusyDialogBatchPost;
	var oGlobalBusyDialogBatchGet;
	var oGlobalBusyDialogToken;
	var token;
	var that;
	return {
		getLatitudeLongitude: function (oModel, aGetNotificationFilters, oView) {
			that = this;
			aLatLongCollection = [];
			oGlobalBusyDialog = new sap.m.BusyDialog();
			oGlobalBusyDialog.open();
			//reading notificationset to get overall longitude and latitude collections(unique) recor
			oModel.read("/NotificationsSet", {
				filters: aGetNotificationFilters,
				success: function (oEvent, results) {
					var aCollection = results.data.results;
					//	alert("length " + aCollection.length);
					var lengthCondition;
					if (aCollection.length > 1000) {
						lengthCondition = 1001;
					} else {
						lengthCondition = aCollection.length;
					}
					//	alert("lengthCondition " + lengthCondition);

					for (var data = 0; data < lengthCondition; data++) {
						//var dataeee = aCollection[0].Latitude;
						aLatLongCollection.push({
							"lattitude": aCollection[data].Latitude,
							"longitude": aCollection[data].Longitude

						});
					}
					// $.each(oEvent.results, function (data, item) {

					// });
					oGlobalBusyDialog.close();
					that._getWeather(aLatLongCollection, oView);
				},
				error: function () {
					oGlobalBusyDialog.close();
				}
			});

		},
		_getWeather: function (aLatLongCollection, oView) {
			var oWeatherInfoModel = new JSONModel();
			//alert("Inside weather");

			aWeatherCollection = [];
			var aGeoServiceCollection = [];
			for (var latlang = 0; latlang < aLatLongCollection.length; latlang++) {
				aGeoServiceCollection.push("@" + aLatLongCollection[latlang].lattitude + "," + aLatLongCollection[latlang].longitude + "");
			}
			var uniqueNames = [];
			$.each(aGeoServiceCollection, function (i, el) {
				if ($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
			});

			var jsonBody = {
				"geodata": uniqueNames,
				"geojson": true,
				"wait": true,
				"fields": "all"
			};
			oGlobalBusyDialogToken = new sap.m.BusyDialog();
			oGlobalBusyDialogToken.open();
			$.ajax({
				type: "POST",
				url: "https://cache14bpa.non-bku.db.de/token?username=api_wetter_db_netz_fahrbahn&password=clearing-fracture-kurt&role=api_wetter",

				contentType: "application/json; charset=utf-8",
				dataType: "json",
				success: function (data) {
					oGlobalBusyDialogToken.close();
					token = data.token;
					oGlobalBusyDialogBatchPost = new sap.m.BusyDialog();
					oGlobalBusyDialogBatchPost.open();
					$.ajax({
						type: "POST",

						url: "https://cache14bpa.non-bku.db.de/extern/weatherdata/is/batch",
						beforeSend: function (xhr) { //Include the bearer token in header
							xhr.setRequestHeader("Authorization", 'Bearer ' + token);
						},
						contentType: 'application/json',
						data: JSON.stringify(jsonBody),

						success: function (response) {
							oGlobalBusyDialogBatchPost.close();

							console.log(response);
							var aObjectWeather = Object.entries(response.data);
							var aArrayData = [];
							//for (var item = 0; item < aObjectWeather.length; item++) {
							aArrayData.push(aObjectWeather[1][1]);
							//}
							var aWeatherCollectionNew = [];
							for (var arrayData = 0; arrayData < aArrayData[0].length; arrayData++) {
								var weatherCollectionJson = {

									lattitude: aArrayData[0][arrayData].geometry.coordinates[1],
									longitude: aArrayData[0][arrayData].geometry.coordinates[0],
									bstid: aArrayData[0][arrayData].properties.bstid,
									gusts: aArrayData[0][arrayData].properties.gusts,
									height: aArrayData[0][arrayData].properties.height,
									rain: aArrayData[0][arrayData].properties.rain + " l/sqm",
									snowfall: aArrayData[0][arrayData].properties.snowfall,
									snowheight: aArrayData[0][arrayData].properties.snowheight,
									stunde: aArrayData[0][arrayData].properties.stunde,
									temperature: aArrayData[0][arrayData].properties.temperature + " °C",
									wind: aArrayData[0][arrayData].properties.wind + " km/h",
								}
								aWeatherCollectionNew.push(weatherCollectionJson);
							}
							//var oWeatherInfoModel = new JSONModel();
							var jsonWeatherModel = new JSONModel({
								"WeatherCollection": aWeatherCollectionNew //pushing aWeatherCollectionNew array into JSON Model
							});
							var aWeatherData = jsonWeatherModel.getData();
							oWeatherInfoModel.setData(aWeatherData);
							oView.setModel(oWeatherInfoModel, "weatherModel")

						},
						error: function () {
							alert("inside batch post fail");
							oGlobalBusyDialogBatchPost.close();
						}
					});

				},
				error: function () {
					oGlobalBusyDialogToken.close();
				}
			});

		},
		getForecastWeather: function (latitude, longitude, oView) {
			$.ajax({
				type: 'GET',
				url: "https://cache14bpa.non-bku.db.de/extern/weatherdata/forecast/@" + latitude + "," + longitude +
					"?si=false&fields=all",
				beforeSend: function (xhr) { //Include the bearer token in header
					xhr.setRequestHeader("Authorization", 'Bearer ' + token);
				},
				success: function (data) {
					var aForeCast = [];

					for (var value = 0; value < data[0].timeslices.length; value++) {
						var forecastData = {
							// "ForeCastCollection": [{
							"timeslices": data[0].timeslices[value],
							"HGT_FL_6H": data[0].HGT_FL_6H[value],
							"HGT_SL_6H": data[0].HGT_SL_6H[value],
							"HUM_REL_6H": data[0].HUM_REL_6H[value],
							"MAX_DIFF_TEMP": data[0].MAX_DIFF_TEMP[value],
							"PREC_6H": data[0].PREC_6H[value],
							"PROB_GUST_60kmh_MAX_6H": data[0].PROB_GUST_60kmh_MAX_6H[value],
							"PROB_GUST_75kmh_MAX_6H": data[0].PROB_GUST_75kmh_MAX_6H[value],
							"PROB_GUST_100kmh_MAX_6H": data[0].PROB_GUST_100kmh_MAX_6H[value],
							"PROB_PREC_MAX_6H": data[0].PROB_PREC_MAX_6H[value],
							"RISK_SNOWBREAKAGE_BEECH_MAX_6H": data[0].RISK_SNOWBREAKAGE_BEECH_MAX_6H[value],
							"RISK_SNOWBREAKAGE_SPRUCE_MAX_6H": data[0].RISK_SNOWBREAKAGE_SPRUCE_MAX_6H[value],
							"RISK_SNOWDRIFT_MAX_6H": data[0].RISK_SNOWDRIFT_MAX_6H[value],
							"RISK_WINDBREAKAGE_BEECH_MAX_6H": data[0].RISK_WINDBREAKAGE_BEECH_MAX_6H[value],
							"RISK_WINDBREAKAGE_SPRUCE_MAX_6H": data[0].RISK_WINDBREAKAGE_SPRUCE_MAX_6H[value],
							"SNOW_DEPTH_MAX_6H": data[0].SNOW_DEPTH_MAX_6H[value],
							"SNOW_FRESH_6H": data[0].SNOW_FRESH_6H[value],
							"TEMP_MAX_6H": data[0].TEMP_MAX_6H[value],
							"TEMP_MIN_6H": data[0].TEMP_MIN_6H[value],
							"WIND_6H": data[0].WIND_6H[value],
							"WIND_DIR_6H": data[0].WIND_DIR_6H[value],
							"WIND_GUST_6H": data[0].WIND_GUST_6H[value]
								// }]
						};
						aForeCast.push(forecastData);
					}

					var jsonModel = new JSONModel({
						"ForeCastSet": aForeCast //pushing aUserGroupList array into JSON Model
					});
					var foreCastData = jsonModel.getData();
					var oForeCastJsonModel = new JSONModel();
					oForeCastJsonModel.setData(foreCastData);
					oView.setModel(oForeCastJsonModel, "oForeCast");

					oView.byId("id_txt_TEMP_MAX_6H")
						.setText(data[0].TEMP_MAX_6H[0]);
					oView.byId("id_txt_TEMP_MIN_6H")
						.setText(data[0].TEMP_MIN_6H[0]);
					oView.byId("id_txt_MAX_DIFF_TEMP")
						.setText(data[0].MAX_DIFF_TEMP[0]);
					oView.byId("id_txt_HUM_REL_6H")
						.setText(data[0].HUM_REL_6H[0]);
					oView.byId("id_txt_PROB_PREC_MAX_6H")
						.setText(data[0].PROB_PREC_MAX_6H[0]);

					oView.byId("id_txt_PREC_6H")
						.setText(data[0].PREC_6H[0]);
					oView.byId("id_txt_SNOW_FRESH_6H")
						.setText(data[0].SNOW_FRESH_6H[0]);
					oView.byId("id_txt_SNOW_DEPTH_MAX_6H")
						.setText(data[0].SNOW_DEPTH_MAX_6H[0]);
					oView.byId("id_txt_RISK_SNOWBREAKAGE_BEECH_MAX_6H")
						.setText(data[0].RISK_SNOWBREAKAGE_BEECH_MAX_6H[0]);
					oView.byId("id_txt_RISK_SNOWBREAKAGE_SPRUCE_MAX_6H")
						.setText(data[0].RISK_SNOWBREAKAGE_SPRUCE_MAX_6H[0]);

					oView.byId("id_txt_RISK_SNOWDRIFT_MAX_6H")
						.setText(data[0].RISK_SNOWDRIFT_MAX_6H[0]);
					oView.byId("id_txt_HGT_FL_6H")
						.setText(data[0].HGT_FL_6H[0]);
					oView.byId("id_txt_HGT_SL_6H")
						.setText(data[0].HGT_SL_6H[0]);
					oView.byId("id_txt_WIND_6H")
						.setText(data[0].WIND_6H[0]);
					oView.byId("id_txt_WIND_GUST_6H")
						.setText(data[0].WIND_GUST_6H[0]);

					oView.byId("id_txt_WIND_DIR_6H")
						.setText(data[0].WIND_DIR_6H[0]);
					oView.byId("id_txt_RISK_WINDBREAKAGE_BEECH_MAX_6H")
						.setText(data[0].RISK_WINDBREAKAGE_BEECH_MAX_6H[0]);
					oView.byId("id_txt_RISK_WINDBREAKAGE_SPRUCE_MAX_6H")
						.setText(data[0].RISK_WINDBREAKAGE_SPRUCE_MAX_6H[0]);
					oView.byId("id_txt_PROB_GUST_100kmh_MAX_6H")
						.setText(data[0].PROB_GUST_100kmh_MAX_6H[0]);
					oView.byId("id_txt_PROB_GUST_75kmh_MAX_6H")
						.setText(data[0].PROB_GUST_75kmh_MAX_6H[0]);
					oView.byId("id_txt_PROB_GUST_60kmh_MAX_6H")
						.setText(data[0].PROB_GUST_60kmh_MAX_6H[0]);

				},

				error: function (error) {
					console.log("FAIL....=================");
				}
			});
		},
		getActualWeather: function (latitude, longitude, oView) {
			var iTemperature;
			var iWind;
			var iGusts;
			var iSnowFall;
			var iRain;
			$.ajax({
				type: 'GET',
				url: "https://cache14bpa.non-bku.db.de/extern/weatherdata/is/@" + latitude + "," + longitude +
					"?si=false&fields=all",
				beforeSend: function (xhr) { //Include the bearer token in header
					xhr.setRequestHeader("Authorization", 'Bearer ' + token);
				},
				success: function (data) {
					for (var temp = 0; temp < data.length; temp++) {
						iTemperature = data[temp].temperature;
						iWind = data[temp].wind;
						iGusts = data[temp].gusts;
						iSnowFall = data[temp].snowfall;
						iRain = data[temp].rain;
					}
					oView.byId("id_txt_temperature")
						.setText(iTemperature + " °C");
					oView.byId("id_txt_wind")
						.setText(iWind + " km/h");
					oView.byId("id_txt_gusts")
						.setText(iGusts + " km/h");
					oView.byId("id_txt_snowfall")
						.setText(iSnowFall + " cm");
					oView.byId("id_txt_rain")
						.setText(iRain + " l/sqm");

				},
				error: function (error) {
					console.log("FAIL....=================");
				}
			});

		},
		getRemovWeather: function (oView) {
			oView.byId("id_spots_temp")
				.removeAllItems();
			oView.byId("id_spots_rain")
				.removeAllItems();
			oView.byId("id_spots_wind")
				.removeAllItems();
		}

	};
});