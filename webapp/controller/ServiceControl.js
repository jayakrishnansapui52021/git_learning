sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"sap/ui/model/Filter",
	"sap/ui/model/json/JSONModel",
	"./Utility"
], function (Controller, MessageToast, Filter, JSONModel, Utility) {
	"use strict";
	var oCGlobalBusyDialog;
	var oRGlobalBusyDialog;
	var oUGlobalBusyDialog;
	var oDGlobalBusyDialog;
	var bIsDefault;
	/*Service control js file used to seperate the service calls from other utility controllers.
	it has a following functions - READ,CREATE,UPDATE and DELETE Operations.*/
	return {
		//getData create function used to set the payload information required for both Create and Update opearions
		getDataCreate: function (bIsFromManageVariant, sVariantName, sDataString, bDefault, sServiceOperation) {

			if (bIsFromManageVariant === true && sServiceOperation === "C") {
				sVariantName = "Standard";
				sDataString = "*standard*";
				bDefault = true;
			}

			var oData = {
				Applid: "ZMAI_UI5_ELAN",
				Variant: sVariantName,
				Filterstring: sDataString,
				Is_Standard: bDefault
			}

			return oData;
		},
		getServiceData: function (oView, sServiceOperation, bIsFromManageVariant, sVariantName, sDataString, bDefault) {
			switch (sServiceOperation) {
			case "C":
				var oData = this.getDataCreate(bIsFromManageVariant, sVariantName, sDataString, bDefault, sServiceOperation);
				this.callCreateService(oData, oView);
				break;

			case "R":

				this.callReadService(oView, sVariantName);
				break;

			case "U":
				var oData = this.getDataCreate(bIsFromManageVariant, sVariantName, sDataString, bDefault, sServiceOperation);
				this.callUpdateService(oData, oView);
				break;

			case "D":
				var oModel = oView.getModel("variant");

				this.callDeleteService(oModel, sVariantName);
				break;

			default:
			}
		},
		//Calling Create Operation
		callCreateService: function (oData, oView) {
			oCGlobalBusyDialog = new sap.m.BusyDialog();
			oCGlobalBusyDialog.open();
			oView.getModel("variant")
				.create("/VariantsSet", oData, {
					success: function (response) {

						MessageToast.show("Erfolgreich erstellt");
						if (oData.Is_Standard === true) {
							oView.byId("id_variant")
								.setDefaultVariantKey(oData.Filterstring);
							oView.byId("id_variant")
								.setInitialSelectionKey(oData.Filterstring);
						}
						oCGlobalBusyDialog.close();
					},
					error: function (response) {
						oCGlobalBusyDialog.close();
						MessageToast.show("Error");
					}
				});
		},
		//Calling Read Operation
		callReadService: function (oView, oModel) {

			var sVariantKey;
			oRGlobalBusyDialog = new sap.m.BusyDialog();
			oRGlobalBusyDialog.open();
			var variantFilter = new Filter(
				"Applid",
				sap.ui.model.FilterOperator.EQ, "ZMAI_UI5_ELAN"
			)
			oModel.read("/VariantsSet", {
				filters: [variantFilter],
				success: function (oEvent) {
					var aVariantKey = [];
					$.each(oEvent.results, function (data, item) {
						//	if (item.Filterstring !== "") {
						aVariantKey.push({
							"filterKey": item.Filterstring,
							"isStandard": item.Is_Standard
						}); //pushing the Filterstring and Is_Standard of VariantsSet into array aVariantKey.
						//	}
					});
					for (var defaultvar = 0; defaultvar < aVariantKey.length; defaultvar++) {
						if (aVariantKey[defaultvar].isStandard === true) {
							sVariantKey = aVariantKey[defaultvar].filterKey;
						}
					}
					oRGlobalBusyDialog.close();
					oView.byId("id_variant")
						.setDefaultVariantKey(sVariantKey);
					oView.byId("id_variant")
						.setInitialSelectionKey(sVariantKey);
					//Triggering the variant selection 
					if (sVariantKey) {
						oView.byId("id_variant")
							.fireSelect({
								key: sVariantKey
							});
					}
				},
				error: function () {
					oRGlobalBusyDialog.close();
				}
			});
		},

		//Calling Update Operation
		callUpdateService: function (oData, oView) {
			oUGlobalBusyDialog = new sap.m.BusyDialog();
			oUGlobalBusyDialog.open();
			oView.getModel("variant")
				.update("/VariantsSet(Sysuser='',Applid='ZMAI_UI5_ELAN',Variant='" + oData.Variant + "')", oData, {
					success: function (response) {

						MessageToast.show("Erfolgreich geupdated");
						if (oData.Is_Standard === true) {
							oView.byId("id_variant")
								.setDefaultVariantKey(oData.Filterstring);
							oView.byId("id_variant")
								.setInitialSelectionKey(oData.Filterstring);
						}
						if (oData.Filterstring === "*standard*") {
							Utility.clearValues(oView);
						} else {
							oView.byId("id_variant")
								.fireSelect({
									key: oData.Filterstring
								});
						}
						oUGlobalBusyDialog.close();
					},
					error: function (response) {
						oUGlobalBusyDialog.close();
						MessageToast.show("Error");
					}
				});
		},
		//Calling Delete Operation - Batch 
		callDeleteService: function (oModel, aVariantName) {
			oDGlobalBusyDialog = new sap.m.BusyDialog();
			oDGlobalBusyDialog.open();
			oModel.setDeferredGroups(["group1"]);
			for (var variant = 0; variant < aVariantName.length; variant++) {
				/*SAPUI5 batch operation for deleting multiple variants at a time.
				All the selected variant will be batched and send into backend.*/
				oModel.remove("/VariantsSet(Sysuser='',Applid='ZMAI_UI5_ELAN',Variant='" + aVariantName[variant] + "')", {
					groupId: "group1",
					changeSetId: "changeSetId1",
					success: function () {
						oDGlobalBusyDialog.close();
						MessageToast.show("Variant wurde gelöscht");
					},
					error: function () {
						oDGlobalBusyDialog.close();
						MessageToast.show("Error in Variant Deletion");
					}
				});
			}
			oModel.submitChanges({
				groupId: "group1"
			});
		}

	};
});