sap.ui.define([
	"sap/ui/core/mvc/Controller",
	 "sap/ui/model/json/JSONModel",
	 	], function (Controller, JSONModel) {
	"use strict";

	return {
		/**
		 * TableControl.js is a Javascript Control to handle the Column Setup for both Order and Notification.
		 * To Avoid the number of lines, it has been seperated from the Krankenblatt Controller.
		 * As Order and Notification having different column, it has been seperated into two different methods to avoid complexity on the krankenblatt controller.
		 *
		 */
		setupColumn: function (oTable, oBundle) {
			var tc_column_01 = new sap.m.Column({
				width: "10em",
				header: new sap.m.Label({
					text: oBundle.getText("groupid")
				})
			});
			oTable.addColumn(tc_column_01);
			var tc_column_02 = new sap.m.Column({
				width: "10em",
				header: new sap.m.Label({
					text: oBundle.getText("groupelement")
				})
			});
			oTable.addColumn(tc_column_02);
			var tc_column_03 = new sap.m.Column({
				width: "10em",
				header: new sap.m.Label({
					text: oBundle.getText("description")
				})
			});
			oTable.addColumn(tc_column_03);
			var tc_column_04 = new sap.m.Column({
				width: "10em",
				header: new sap.m.Label({
					text: oBundle.getText("startdate")
				})
			});
			oTable.addColumn(tc_column_04);
			var tc_column_05 = new sap.m.Column({
				width: "10em",
				header: new sap.m.Label({
					text: oBundle.getText("enddate")
				})
			});
			oTable.addColumn(tc_column_05);
			var tc_column_06 = new sap.m.Column({
				width: "10em",
				header: new sap.m.Label({
					text: "Seqno"
				})
			});
			oTable.addColumn(tc_column_06);
		},
		setupTableColumnItem: function () {
			debugger;
			var columnListItemOrder = new sap.m.ColumnListItem({
				type: "Active"
			});
			var tableCell1 = new sap.m.Text({
				text: "{elan>GroupId}"
			});
			columnListItemOrder.addCell(tableCell1);
			var tableCell2 = new sap.m.Text({
				text: "{elan>GroupingElementid}"
			});
			columnListItemOrder.addCell(tableCell2);
			var tableCell3 = new sap.m.Text({
				text: "{elan>Description}"
			});
			columnListItemOrder.addCell(tableCell3);
			var tableCell4 = new sap.m.Text({
				text: "{elan>Startts}"
			});
			columnListItemOrder.addCell(tableCell4);
			var tableCell5 = new sap.m.Text({
				text: "{elan>Endets}"
			});
			columnListItemOrder.addCell(tableCell5);
			var tableCell6 = new sap.m.Text({
				text: "{elan>Seqno}"
			});
			columnListItemOrder.addCell(tableCell6);
			debugger;
			return columnListItemOrder;
		}

	};
});