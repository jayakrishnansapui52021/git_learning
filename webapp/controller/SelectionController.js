sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"./Utility"
], function (Controller, MessageToast, Utility) {
	"use strict";
	var aSelectedTypes;
	var aSelectedPriorities;
	var aSelectedStatus;
	var aSelectedPlants;
	var aSelectedWPlace;
	var aSelectedTplace;
	var dFromDate;
	var dToDate;
	var dFormattedStartDate;
	var dFormattedEndDate;
	var aNotificationType;
	var aNotificationPriority;
	var aNotificationStatus;
	var aPlant;
	var aWorkPlace;
	var aTplace;
	var aFilterArg;
	var aSelectedRailObj;
	var aRailObjects;

	return {
		/*	getSelection method is used to get all the selected values either from manual or variant.
			pasre the required values and convert it into Json Array to pass to next view in router arguments
			and done the validation for plant and notification type selection*/
		//getSelectionItems is a private method used in this controller to get the selected filter items from the 
		//selection screen
		_getSelectedItems: function (oView) {
			aSelectedTypes = oView.byId("id_mc_type").getSelectedItems();
			aSelectedPriorities = oView.byId("id_mc_priority").getSelectedItems();
			aSelectedStatus = oView.byId("id_mc_status").getSelectedItems();
			aSelectedPlants = oView.byId("id_mi_plant").getTokens();
			aSelectedWPlace = oView.byId("id_mi_workplace").getTokens();
			aSelectedTplace = oView.byId("id_mi_tplace").getTokens();
			dFromDate = oView.byId("id_dr_date").getDateValue();
			dToDate = oView.byId("id_dr_date").getSecondDateValue();
			aSelectedRailObj = oView.byId("id_mc_rail_object").getSelectedItems();
			
		},
		getSelection: function (oView, oBundle, bIsFromVariant, sVariantName) {
			this._getSelectedItems(oView);
			dFormattedStartDate = Utility.getDateFormatter(bIsFromVariant, dFromDate);
			dFormattedEndDate = Utility.getDateFormatter(bIsFromVariant, dToDate);
			this._getOverAllCollection();
			if ((aNotificationType && aNotificationType.length > 0) && (aPlant && aPlant.length > 0)) {
				aFilterArg = [];
				aFilterArg.push(aNotificationType);
				aFilterArg.push(aNotificationPriority);
				aFilterArg.push(aNotificationStatus);
				aFilterArg.push(dFormattedStartDate);
				aFilterArg.push(dFormattedEndDate);
				aFilterArg.push(aPlant);
				aFilterArg.push(aWorkPlace);
				aFilterArg.push(aTplace);
				aFilterArg.push(sVariantName)
				aFilterArg.push(aRailObjects);
				if (oView.byId("id_check_weather").getSelected() === true) {
					aFilterArg.push(true);
				} else {
					aFilterArg.push(false);
				}
			} else if (!aNotificationType || aNotificationType.length === 0) {
				MessageToast.show(oBundle
					.getText("toast_notiftype_error"));
				oView.byId("id_mi_plant").setValueState(sap.ui.core.ValueState.None);
				oView.byId("id_mc_type").setValueState(sap.ui.core.ValueState.Error);
			} else {
				MessageToast.show(oBundle
					.getText("toast_plant_error"));
				oView.byId("id_mc_type").setValueState(sap.ui.core.ValueState.None);
				oView.byId("id_mi_plant").setValueState(sap.ui.core.ValueState.Error);
			}
			return aFilterArg;
		},
		_getOverAllCollection: function () {
			aNotificationType = [];
			aNotificationPriority = [];
			aNotificationStatus = [];
			aPlant = [];
			aWorkPlace = [];
			aTplace = [];
			aRailObjects = [];

			for (var type = 0; type < aSelectedTypes.length; type++) {
				aNotificationType.push(aSelectedTypes[type].getKey());
			}
			for (var rail = 0; rail < aSelectedRailObj.length; rail++) {
				aRailObjects.push(aSelectedRailObj[rail].getKey());
			}
			for (var priority = 0; priority < aSelectedPriorities.length; priority++) {
				aNotificationPriority.push(aSelectedPriorities[priority].getKey());
			}
			if (aSelectedStatus.length === 0) {
				aNotificationStatus.push("MOFN");
				aNotificationStatus.push("MMAB");
			} else {
				for (var status = 0; status < aSelectedStatus.length; status++) {
					aNotificationStatus.push(aSelectedStatus[status].getKey());
				}
			}
			for (var plant = 0; plant < aSelectedPlants.length; plant++) {
				aPlant.push(aSelectedPlants[plant].getText());
			}
			for (var wplace = 0; wplace < aSelectedWPlace.length; wplace++) {
				aWorkPlace.push(aSelectedWPlace[wplace].getText());
			}
			for (var tplace = 0; tplace < aSelectedTplace.length; tplace++) {
				aTplace.push(aSelectedTplace[tplace].getText());
			}
		}

	};
});