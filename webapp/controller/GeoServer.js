sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"sap/ui/model/Filter",
	"sap/ui/model/json/JSONModel",
	"com/db/de/elan/gis/Hektometer",
	"com/db/de/elan/gis/Crossing",
	"com/db/de/elan/gis/Signal",
	"com/db/de/elan/gis/Smallbridge",
	"com/db/de/elan/gis/Bigbridge",
	"com/db/de/elan/gis/Wall",
	"com/db/de/elan/gis/Tracklock",
	"com/db/de/elan/gis/Turnout",
	"com/db/de/elan/gis/Indusis"
], function (Controller, MessageToast, Filter, JSONModel, Hektometer, Crossing, Signal, Smallbridge, Bigbridge, Wall, Tracklock, Turnout,
	Indusis) {
	"use strict";
	//var DOMAIN_GEO_SERVER = "https://geovdbn.intranet.deutschebahn.com/pgv-map/geoserver.action?";
	var DOMAIN_GEO_SERVER = "https://geovdbn-test2.intranet-test.deutschebahn.com/geoserver/GeoS_ext_intra/ows?";
	return {
		getGeoServerData: function (aObject, oView, bBoxValues) {
			var aOverAllRailObjects = ["hectometer", "crossing", "signal", "small_bridges", "big_bridges", "walls", "track_locks", "indusis",
				"turnout", "ivl"
			];
			var aUnselectedObjects = aOverAllRailObjects.filter(function (item) {
				return !aObject.includes(item);
			})
			for (var remove = 0; remove < aUnselectedObjects.length; remove++) {
				switch (aUnselectedObjects[remove]) {
				case "hectometer":
					oView.byId("id_spots_hectometer")
						.removeAllItems();
					break;
				case "signal":
					oView.byId("id_spots_signal")
						.removeAllItems();
					break;
				case "crossing":
					oView.byId("id_spots_crossings")
						.removeAllItems();
					break;
				case "small_bridges":
					oView.byId("id_spots_small_bridge")
						.removeAllItems();
					oView.byId("id_route_small_bridge")
						.removeAllItems();
					break;
				case "big_bridges":
					oView.byId("id_spots_big_bridge")
						.removeAllItems();
					oView.byId("id_route_big_bridge")
						.removeAllItems();
					break;
				case "walls":
					oView.byId("id_spots_walls")
						.removeAllItems();
					oView.byId("id_route_walls")
						.removeAllItems();
					break;
				case "track_locks":
					oView.byId("id_spots_tl")
						.removeAllItems();
					break;
				case "indusis":
					oView.byId("id_spots_in")
						.removeAllItems();
					break;
				case "turnout":
					oView.byId("id_spots_to")
						.removeAllItems();
					break;

				default:
				}
			}
			for (var object = 0; object < aObject.length; object++) {
				
				switch (aObject[object]) {
				case "hectometer":
					if (bBoxValues.length > 0) {
						var sGeoServiceHectoMeterURL =
							DOMAIN_GEO_SERVER +
							"service=WFS&version=1.1.0&request=GetFeature&outputFormat=application/json&typeName=GeoS_ext_intra:SPI_V_GEO_HEKTOMETERPUNKTE&propertyName=GEOMETRIE,ID,LADE_ID,ZEITSCHEIBE,STRNR,STRE_ID&maxFeatures=1000&bbox=" +
							bBoxValues[0] + "";
					} else {
						var sGeoServiceHectoMeterURL =
							DOMAIN_GEO_SERVER +
							"service=WFS&version=1.1.0&request=GetFeature&outputFormat=application/json&typeName=GeoS_ext_intra:SPI_V_GEO_HEKTOMETERPUNKTE&propertyName=GEOMETRIE,ID,LADE_ID,ZEITSCHEIBE,STRNR,STRE_ID&maxFeatures=1000";
					}
					Hektometer.callHectoMeterService(sGeoServiceHectoMeterURL, oView);
					break;
				case "signal":
					if (bBoxValues.length > 0) {
						var sGeoServiceSignalURL =
							DOMAIN_GEO_SERVER +
							"service=WFS&version=1.1.0&request=GetFeature&outputFormat=application/json&typeName=GeoS_ext_intra:SPI_V_GEO_SIGNALE&propertyName=GEOMETRIE,SIG_ID,LADE_ID,ZEITSCHEIBE,SIG_BEZEICHNUNG,SIG_TECHN_PLATZ&maxFeatures=1000&bbox=" +
							bBoxValues[0] + ""
					} else {
						var sGeoServiceSignalURL =
							DOMAIN_GEO_SERVER +
							"service=WFS&version=1.1.0&request=GetFeature&outputFormat=application/json&typeName=GeoS_ext_intra:SPI_V_GEO_SIGNALE&propertyName=GEOMETRIE,SIG_ID,LADE_ID,ZEITSCHEIBE,SIG_BEZEICHNUNG,SIG_TECHN_PLATZ&maxFeatures=1000"
					}
					Signal.callSignalService(sGeoServiceSignalURL, oView);
					break;
				case "crossing":
					if (bBoxValues.length > 0) {
						var sGeoServiceCrossingURL =
							DOMAIN_GEO_SERVER +
							"service=WFS&version=1.1.0&request=GetFeature&outputFormat=application/json&typeName=GeoS_ext_intra:SPI_V_GEO_BAHNUEBERGAENGE&propertyName=GEOMETRIE_PUNKT,GEOMETRIE,ID,LADE_ID,ZEITSCHEIBE,BUEB_ID,TECHNPLATZ&maxFeatures=1000&bbox=" +
							bBoxValues[0] + ""
					} else {
						var sGeoServiceCrossingURL =
							DOMAIN_GEO_SERVER +
							"service=WFS&version=1.1.0&request=GetFeature&outputFormat=application/json&typeName=GeoS_ext_intra:SPI_V_GEO_BAHNUEBERGAENGE&propertyName=GEOMETRIE_PUNKT,GEOMETRIE,ID,LADE_ID,ZEITSCHEIBE,BUEB_ID,TECHNPLATZ&maxFeatures=1000"
					}

					Crossing.callCrossingService(sGeoServiceCrossingURL, oView);
					break;
				case "small_bridges":
					if (bBoxValues.length > 0) {
						var sGeoServiceSmallBridgeURL =
							DOMAIN_GEO_SERVER +
							"service=WFS&version=1.1.0&request=GetFeature&outputFormat=application/json&typeName=GeoS_ext_intra:SPI_V_GEO_BRUECKEN_KL&propertyName=GEOMETRIE,ID,LADE_ID,ZEITSCHEIBE,BEZEICHNUNG,TECHNPLATZ&maxFeatures=1000&bbox=" +
							bBoxValues[0] + "";
					} else {
						var sGeoServiceSmallBridgeURL =
							DOMAIN_GEO_SERVER +
							"service=WFS&version=1.1.0&request=GetFeature&outputFormat=application/json&typeName=GeoS_ext_intra:SPI_V_GEO_BRUECKEN_KL&propertyName=GEOMETRIE,ID,LADE_ID,ZEITSCHEIBE,BEZEICHNUNG,TECHNPLATZ&maxFeatures=1000";
					}
					Smallbridge.callSmallBridgeService(sGeoServiceSmallBridgeURL, oView);
					break;
				case "big_bridges":
					if (bBoxValues.length > 0) {
						var sGeoBigBridgeServiceURL =
							DOMAIN_GEO_SERVER +
							"service=WFS&version=1.1.0&request=GetFeature&outputFormat=application/json&typeName=GeoS_ext_intra:SPI_V_GEO_BRUECKEN_GR&propertyName=GEOMETRIE,ID,LADE_ID,ZEITSCHEIBE,BRUE_ID,TECHNPLATZ&maxFeatures=1000&bbox=" +
							bBoxValues[0] + "";
					} else {
						var sGeoBigBridgeServiceURL =
							DOMAIN_GEO_SERVER +
							"service=WFS&version=1.1.0&request=GetFeature&outputFormat=application/json&typeName=GeoS_ext_intra:SPI_V_GEO_BRUECKEN_GR&propertyName=GEOMETRIE,ID,LADE_ID,ZEITSCHEIBE,BRUE_ID,TECHNPLATZ&maxFeatures=1000";
					}
					Bigbridge.callBigBridgeService(sGeoBigBridgeServiceURL, oView);
					break;
				case "walls":
					if (bBoxValues.length > 0) {
						var sGeoServiceWallURL =
							DOMAIN_GEO_SERVER +
							"service=WFS&version=1.1.0&request=GetFeature&outputFormat=application/json&typeName=GeoS_ext_intra:SPI_V_GEO_SCHUTZWAND_LINIE_J&propertyName=GEOMETRIE,ID,LADE_ID,ZEITSCHEIBE,BEZEICHNUNG,TECHNPLATZ&maxFeatures=1000&bbox=" +
							bBoxValues[0] + "";
					} else {
						var sGeoServiceWallURL =
							DOMAIN_GEO_SERVER +
							"service=WFS&version=1.1.0&request=GetFeature&outputFormat=application/json&typeName=GeoS_ext_intra:SPI_V_GEO_SCHUTZWAND_LINIE_J&propertyName=GEOMETRIE,ID,LADE_ID,ZEITSCHEIBE,BEZEICHNUNG,TECHNPLATZ&maxFeatures=1000";
					}
					Wall.callWallService(sGeoServiceWallURL, oView);
					break;
				case "track_locks":
					if (bBoxValues.length > 0) {
						var sGeoServiceTrackLockURL =
							DOMAIN_GEO_SERVER +
							"service=WFS&version=1.1.0&request=GetFeature&outputFormat=application/json&typeName=GeoS_ext_intra:SPI_V_GEO_GLEISSPERREN&propertyName=GEOMETRIE,GSP_ID,LADE_ID,ZEITSCHEIBE,GSP_NUMMER,GSP_TECHN_PLATZ&maxFeatures=1000&bbox=" +
							bBoxValues[0] + "";
					} else {
						var sGeoServiceTrackLockURL =
							DOMAIN_GEO_SERVER +
							"service=WFS&version=1.1.0&request=GetFeature&outputFormat=application/json&typeName=GeoS_ext_intra:SPI_V_GEO_GLEISSPERREN&propertyName=GEOMETRIE,GSP_ID,LADE_ID,ZEITSCHEIBE,GSP_NUMMER,GSP_TECHN_PLATZ&maxFeatures=1000";
					}
					Tracklock.callTrackLockService(sGeoServiceTrackLockURL, oView);
					break;
				case "indusis":
					if (bBoxValues.length > 0) {
						var sGeoServiceIndusisURL =
							DOMAIN_GEO_SERVER +
							"service=WFS&version=1.1.0&request=GetFeature&outputFormat=application/json&typeName=GeoS_ext_intra:SPI_V_GEO_INDUSI&propertyName=GEOMETRIE,RS_ID,LADE_ID,ZEITSCHEIBE,RS_WL_BAUART,RS_TECHN_PLATZ&maxFeatures=1000&bbox=" +
							bBoxValues[0] + "";
					} else {
						var sGeoServiceIndusisURL =
							DOMAIN_GEO_SERVER +
							"service=WFS&version=1.1.0&request=GetFeature&outputFormat=application/json&typeName=GeoS_ext_intra:SPI_V_GEO_INDUSI&propertyName=GEOMETRIE,RS_ID,LADE_ID,ZEITSCHEIBE,RS_WL_BAUART,RS_TECHN_PLATZ&maxFeatures=1000";
					}
					Indusis.callIndusisService(sGeoServiceIndusisURL, oView);
					break;
				case "turnout":
					if (bBoxValues.length > 0) {
						var sGeoServiceTurnOutURL =
							DOMAIN_GEO_SERVER +
							"service=WFS&version=1.1.0&request=GetFeature&outputFormat=application/json&typeName=GeoS_ext_intra:SPI_V_GEO_WEICHE_KREUZUNG&propertyName=ID,BEZEICHNUNG,GEO_PUNKT_WGS84_2D,TECHN_PLATZ,GIS_SEGMENT,PK,STRECKENNUMMER&maxFeatures=1000&bbox=" +
							bBoxValues[0] + "";
					} else {
						var sGeoServiceTurnOutURL =
							DOMAIN_GEO_SERVER +
							"service=WFS&version=1.1.0&request=GetFeature&outputFormat=application/json&typeName=GeoS_ext_intra:SPI_V_GEO_WEICHE_KREUZUNG&propertyName=ID,BEZEICHNUNG,GEO_PUNKT_WGS84_2D,TECHN_PLATZ,GIS_SEGMENT,PK,STRECKENNUMMER&maxFeatures=1000";
					}
					Turnout.callTurnOutService(sGeoServiceTurnOutURL, oView);
					break;
					// case "ivl":
					// 	var sPosition
					// 	if (bBoxValues.length > 0) {
					// 		sPosition = bBoxValues[1];
					// 		var aBBOX = bBoxValues[0].split(",");
					// 		var aNewBBox = [];
					// 		aNewBBox.push(aBBOX[1]);
					// 		aNewBBox.push(aBBOX[0]);
					// 		aNewBBox.push(aBBOX[3]);
					// 		aNewBBox.push(aBBOX[2]);
					// 		var sBoundedValue = aNewBBox.join();
					// 		var sIVLGeoServiceURL =
					// 			"https://geovdbn-test2.intranet-test.deutschebahn.com/geoserver/GeoS_ext_intra/ows?service=WMS&version=1.1.0&request=GetMap&layers=HINTERGRUNDKARTEN:ivl_plaene&styles=&bbox=" +
					// 			sBoundedValue + "&width=608&height=768&srs=EPSG:4326&format=image/png&transparent=true&bgcolor=0xFFFFFF";
					// 	} else {
					// 		sPosition = "10.522845;51.167417;0";
					// 		var sIVLGeoServiceURL =
					// 			"https://geovdbn-test2.intranet-test.deutschebahn.com/geoserver/GeoS_ext_intra/ows?service=WMS&version=1.1.0&request=GetMap&layers=HINTERGRUNDKARTEN:ivl_plaene&styles=&bbox=10.51027,51.15484,10.53542,51.17999&width=608&height=768&srs=EPSG:4326&format=image/png&transparent=true&bgcolor=0xFFFFFF";
					// 	}
					// 	console.log("sIVLGeoServiceURL --> " + sIVLGeoServiceURL);
					// 	this._callIVLService(sIVLGeoServiceURL, oView, sPosition, sBoundedValue);
					// 	break;
				default:
				}
			}

		}

	};
});